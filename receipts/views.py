from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

@login_required
def home(request):
   receipts1 = Receipt.objects.filter(purchaser=request.user)
   context = {
      "receipts2": receipts1
   } 
   return render(request, "receipts/receipts3.html", context)
@login_required
def create_receipt(request):
   if request.method == "POST":
      form = ReceiptForm(request.POST)
      if form.is_valid():
         receipt4 = form.save(False)
         receipt4.purchaser = request.user
         receipt4.save()
         return redirect("home")
   else:
      form = ReceiptForm()
   context = {
      "form5": form
   }
   return render(request, "receipts/create.html", context)
@login_required
def category_list(request):
   cat = ExpenseCategory.objects.filter(owner=request.user)
   context = {
      "cat2": cat
   }
   return render(request, "categories/category_list.html", context)
@login_required
def account_list(request):
   acc = Account.objects.filter(owner=request.user)
   context = {
      "acc2": acc
   }
   return render(request, "accounts/account_list.html", context)
@login_required
def create_category(request):
   if request.method == "POST":
      form = ExpenseCategoryForm(request.POST)
      if form.is_valid():
         cc1 = form.save(False)
         cc1.owner = request.user
         cc1.save()
         return redirect("category_list")
   else:
      form = ExpenseCategoryForm()
   context = {
      "cc2": form,
   }
   return render(request, "categories/create_category.html", context)
@login_required
def create_account(request):
   if request.method == "POST":
      form = AccountForm(request.POST)
      if form.is_valid():
         ca3 = form.save(False)
         ca3.owner = request.user
         ca3.save()
         return redirect("account_list")
   else:
      form = AccountForm()
   context = {
         "ca4": form
   }
   return render(request, "accounts/create_account.html", context)   
